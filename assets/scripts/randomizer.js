const minSpanElement = document.getElementById("min_span");
const maxSpanElement = document.getElementById("max_span");
const minInputElement = document.getElementById("min_input");
const maxInputElement = document.getElementById("max_input");
const singleButtonElement = document.getElementById("single");
const automaticButtonElement = document.getElementById("automatic");
// const cycleSpanElement = document.getElementById("cycle_span");
// const cycleInputElement = document.getElementById("cycle_input");
const resetBtnElement = document.getElementById("reset");
const cardDivElement = document
	.querySelector(".card")
	.querySelectorAll(":scope >div");

// random number
const numRandomElement = document.getElementById("num_random");

// minSpanElement.textContent = minInputElement.value;
maxSpanElement.textContent = maxInputElement.value;
// cycleSpanElement.textContent = cycleInputElement.value;

let minValue = minSpanElement.textContent;
let maxValue = maxInputElement.value;
// let cycleValue = cycleInputElement.value;
let resultArray = [];

minValue = minSpanElement.textContent;
let randomTimerId = null;
let randomRenderIndex = 0;

const randomNumberAnimation = () => {
	if (randomTimerId) {
		clearInterval(randomTimerId);
	}
	if (randomRenderIndex < 6) {
		randomTimerId = setInterval(() => {
			let num = Math.ceil(Math.random() * maxValue);

			if (!cardDivElement[randomRenderIndex]) {
				if (randomTimerId) {
					clearInterval(randomTimerId);
				}
				return;
			}
			cardDivElement[randomRenderIndex].textContent = num;
		}, 100);
	}
};

const renderValue = () => {
	let index = 0;
	if (randomTimerId) {
		clearInterval(randomTimerId);
		randomRenderIndex++;
	}

	cardDivElement.forEach((div) => {
		// if (index < resultArray.length) {
		div.textContent = resultArray[index] ? resultArray[index] : "??";
		// }
		index++;
	});
};

// const cycleValueHandler = (e) => {
// 	cycleSpanElement.textContent = e.target.value;
// 	cycleValue = e.target.value;
// };

const changeValueHandler = (e) => {
	if (!e.target.value || e.target.value <= 0) {
		maxSpanElement.textContent = 1;
	} else {
		maxSpanElement.textContent = e.target.value;
	}

	maxValue = maxSpanElement.textContent;
};

const singleRandomNumberHandler = (e) => {
	const cycleValue = Math.ceil(Math.random() * 10);
	let copyOfCycleValue = cycleValue;
	let timerId = null;
	randomNumberAnimation();
	const cycleRandomHandler = () => {
		clearTimeout(timerId);
		let randomNum = Math.ceil(Math.random() * maxValue);
		if (randomNum < 10) {
			randomNum = "0" + randomNum;
		}

		if (resultArray.length < 6) {
			const found = resultArray.includes(randomNum);
			copyOfCycleValue--;

			if (found) {
				if (randomTimerId) {
					clearInterval(randomTimerId);
				}
				singleRandomNumberHandler();
				return;
			}
			if (copyOfCycleValue === 0) {
				resultArray.push(randomNum);
				renderValue();
			}
			if (copyOfCycleValue > 0) {
				timerId = setTimeout(() => {
					cycleRandomHandler();
				}, 500);
			}
		}
	};
	timerId = setTimeout(() => {
		cycleRandomHandler();
	}, 1000);
};

const automaticRandomNumberHandler = () => {
	let intervalId;

	// ! newScript
	singleRandomNumberHandler();

	intervalId = setInterval(() => {
		// if (intervalId) {
		// 	clearInterval(intervalId);
		// }
		if (resultArray.length < 6) {
			singleRandomNumberHandler();
		}
		if (resultArray.length >= 6) {
			clearintervalHandler();
		}
	}, 5000);

	const clearintervalHandler = () => {
		clearInterval(intervalId);
	};

	// ! interval
	// intervalId = setInterval(() => {
	// 	interval();
	// 	console.log("here");
	// }, 4000);

	// const interval = () => {
	// 	singleRandomNumberHandler();
	// 	if (resultArray.length >= 6) {
	// 		clearInterval(intervalId);
	// 		return;
	// 	}
	// };
	// interval();

	// ? recursion
	// let sequence = 6;
	// const recursionFunction = () => {
	// const callSingleHandler = () => {
	// 	singleRandomNumberHandler();
	// 	sequence--;
	// };
	// callSingleHandler();
	// if (sequence > 0) {
	// 	recursionFunction();
	// }
	// console.log(resultArray);
	// };
	// recursionFunction();
};

const resetHandler = () => {
	resultArray = [];
	renderValue();
	maxInputElement.value = 42;
	maxSpanElement.textContent = maxInputElement.value;
	randomRenderIndex = 0;
	if (randomTimerId) {
		clearInterval(randomTimerId);
	}
};

// ? single button element
automaticButtonElement.addEventListener("click", automaticRandomNumberHandler);
singleButtonElement.addEventListener("click", singleRandomNumberHandler);

// ? max input element

maxInputElement.addEventListener("click", changeValueHandler);
maxInputElement.addEventListener("change", changeValueHandler);
maxInputElement.addEventListener("input", changeValueHandler);

// cycleInputElement.addEventListener("click", cycleValueHandler);
// cycleInputElement.addEventListener("change", cycleValueHandler);
// cycleInputElement.addEventListener("input", cycleValueHandler);

resetBtnElement.addEventListener("click", resetHandler);
