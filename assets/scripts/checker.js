const inputDivElements = document
	.getElementById("input")
	.querySelectorAll(":scope > input");
const outputDivElements = document
	.getElementById("output")
	.querySelectorAll(":scope > input");
const submitBtnElement = document.getElementById("submit");
const h1Element = document.getElementById("display").querySelector(":scope>h1");

let inputArray = [];
let outputArray = [];

const addClassHandler = (inputIndex, outputIndex) => {
	inputDivElements[inputIndex].classList.add("matched");
	outputDivElements[outputIndex].classList.add("matched");
};

const matchArrayHandler = () => {
	removeInputClassHandler();
	removeOutputClassHandler();
	let totalMatched = 0;
	let indexOfOutputMatched = [];
	let indexOfInputMatched = [];
	outputArray.forEach((out, index) => {
		const found = inputArray.includes(out);
		if (found) {
			const inputIndex = inputArray.indexOf(out);
			indexOfInputMatched.push(inputIndex);
			indexOfOutputMatched.push(index);
			totalMatched++;
			addClassHandler(inputIndex, index);
		}
	});
	h1Element.textContent = totalMatched;
};

const checkIfDoubleEntry = (element, array, num, index) => {
	const doubleEntryIndex = array.indexOf(num);
	element[doubleEntryIndex].classList.add("doubleEntry");
	element[index].classList.add("doubleEntry");
};
const checkIfEntryBelowOne = (element, array, num, index) => {
	element[index].classList.add("doubleEntry");
};

const outputChangeHandler = (index, e) => {
	const found = outputArray.includes(e.target.value);
	if (found) {
		checkIfDoubleEntry(outputDivElements, outputArray, e.target.value, index);
		return;
	}
	if (e.target.value < 1) {
		checkIfEntryBelowOne(outputDivElements, outputArray, e.target.value, index);
		return;
	}
	outputArray[index] = e.target.value;
};

const inputChangeHandler = (index, e) => {
	const found = inputArray.includes(e.target.value);
	if (found) {
		checkIfDoubleEntry(inputDivElements, inputArray, e.target.value, index);
		return;
	}

	if (e.target.value < 1) {
		checkIfEntryBelowOne(inputDivElements, inputArray, e.target.value, index);
		return;
	}
	inputArray[index] = e.target.value;
};
const removeInputClassHandler = () => {
	inputDivElements.forEach((div) => {
		div.removeAttribute("class");
	});
};

const removeOutputClassHandler = () => {
	outputDivElements.forEach((div) => {
		div.removeAttribute("class");
	});
};

inputDivElements.forEach((div, index) => {
	div.addEventListener("input", (e) => inputChangeHandler(index, e));
});

outputDivElements.forEach((div, index) => {
	div.addEventListener("input", (e) => outputChangeHandler(index, e));
});

submitBtnElement.addEventListener("click", matchArrayHandler);
